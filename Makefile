# Build rules for DFSG FAQ.
#
# Derived from https://salsa.debian.org/debian/developers-reference/-/blob/master/Makefile
#
# This build script works on the bare un-tared upstream tarball, the
# checked-out git repository, and the patch-applied Debian source package
# source tree.

# Basic package information
ifneq ("$(wildcard debian/changelog)","")
# Patch-applied Debian source package source tree
PACKAGE   ?= $(shell dpkg-parsechangelog -SSource)
TIMESTAMP ?= $(shell dpkg-parsechangelog -STimestamp)
DATE      ?= $(shell date -u -d '@$(TIMESTAMP)' +'%Y-%m-%d')
VERSION   ?= $(shell dpkg-parsechangelog -SVersion)
else ifneq ("$(wildcard .git)","")
# Checked-out git repository
PACKAGE   ?= $(basename $(notdir $(shell git config --get remote.origin.url)))
TIMESTAMP ?= $(shell git show -s --format=%ct HEAD)
DATE      ?= $(shell date -u -d '@$(TIMESTAMP)' +'%Y-%m-%d')
VERSION   ?= $(shell git describe --tags)
# Adjust version as needed: debian/% --> %
VERSION   := $(patsubst debian/%, %, $(VERSION))
else
# Un-tared upstream tarball
PACKAGE   ?= $(notdir $(CURDIR))
TIMESTAMP ?= $(shell date +%s)
DATE      ?= $(shell date -u -d '@$(TIMESTAMP)' +'%Y-%m-%d')
VERSION   ?= 0.0~$(subst -,.,$(DATE))
endif

export PACKAGE TIMESTAMP DATE VERSION

# Source file directory
SOURCE_DIR  = source

# Building files under language specific subdirectories of $(BUILD_DIR)
BUILD_DIR   = build

# Match this with conf.py: locale_dir = ["locales"]
LOCALE_DIR = locales

# All languages
LANGS       ?= en $(notdir $(wildcard $(SOURCE_DIR)/$(LOCALE_DIR)/*))
# All languages without PDF building
LANGS_NOPDF = 

# All output formats
FORMATS     ?= text html # singlehtml

# Conversion programs to use.  Export these so that they're visible to
# submakes.
export SPHINX     = sphinx-build
export SPHINXINTL = sphinx-intl

# Installation directories.  Normally this is only used by debian/rules
# build, which will set DESTDIR to put the installation under the temporary
# Debian packaging directory.
prefix      = /usr
datarootdir = $(prefix)/share
datadir     = $(datarootdir)/$(PACKAGE)
infodir     = $(datarootdir)/info

# Order of source file basenames in $(SOURCE_DIR)/index.rst
BASE_ORDER    := $(shell grep -e '^   [^:]' $(SOURCE_DIR)/index.rst)

# Source file basenames found as *.rst
BASE_FILTER   := $(basename $(notdir $(wildcard $(SOURCE_DIR)/*.rst)))

# Ordered source *.rst file basenames to build $(PACKAGE)
BASE_FILES   := $(filter $(BASE_FILTER), $(BASE_ORDER))

SOURCE_FILES := $(foreach f,$(BASE_FILES),				\
		$(SOURCE_DIR)/$(f).rst)		 			\
		$(SOURCE_DIR)/conf.py

PO_FILES     := $(foreach l,$(filter-out en,$(LANGS)),			\
		$(foreach f,$(BASE_FILES),				\
		$(SOURCE_DIR)/$(LOCALE_DIR)/$(l)/LC_MESSAGES/$(f).po))

# General build targets.  These are the ones a human may build from the
# command line, or that are used by the Debian build system.
#
# Checks for PO files are skipped since $(SPHINX) takes care of them.
#

all: $(FORMATS)

# A list of plain text files for all $(LANGS)
TEXT_FILES   := $(foreach l,$(LANGS),					\
		$(BUILD_DIR)/$(l)/text/$(PACKAGE).txt)

text: $(TEXT_FILES)

# Somehow -M doesn't work well, so use -b
$(BUILD_DIR)/%/text/$(PACKAGE).txt: $(SOURCE_FILES)
	rm -f $@
	$(SPHINX) -b text -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)
	# Make $(PACKAGE).txt
	cp $(@D)/index.txt $@
	set -e;								\
	    for f in $(BASE_FILES); do					\
		printf "\n\n\n" >>$@;					\
		cat $(@D)/"$$f".txt >>$@;				\
	    done

# A list of PDF files for all valid languages
PDF_FILES := $(foreach l,$(filter-out $(LANGS_NOPDF),$(LANGS)),		\
		$(BUILD_DIR)/$(l)/pdf/$(PACKAGE).pdf)
pdf: $(PDF_FILES)

# Somehow -M doesn't work well, so use -b
$(BUILD_DIR)/%/pdf/$(PACKAGE).pdf: $(SOURCE_FILES)
	$(SPHINX) -b latex -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)
	$(MAKE) -C $(@D)

# A list of place-holder PDF files for all non-valid languages
NOPDF_FILES  := $(foreach l,$(LANGS_NOPDF),				\
		$(BUILD_DIR)/$(l)/nopdf/$(PACKAGE).pdf)
nopdf: $(NOPDF_FILES)

# Build nopdf place holder PDF files with latexmk
$(BUILD_DIR)/%/nopdf/$(PACKAGE).pdf: nopdf.tex
	mkdir -p $(@D)
	sed -e "s/@lang@/$*/g" nopdf.tex >$(@D)/nopdf.tex
	set -e; cd $(@D); latexmk -pdf -dvi- -ps- $<
	mv $(@D)/nopdf.pdf $(@)

# A list of epub files for all $(LANGS)
EPUB_FILES   := $(foreach l,$(LANGS),					\
		$(BUILD_DIR)/$(l)/epub/$(PACKAGE).epub)
epub: $(EPUB_FILES)

# Somehow -M doesn't work well, so use -b
$(BUILD_DIR)/%/epub/$(PACKAGE).epub: $(SOURCE_FILES)
	$(SPHINX) -b epub -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)

# A list of info files for all $(LANGS)
# Please note, translated info files are installed as $(PACKAGE)-$(l).info:
INFO_FILES   := $(foreach l,$(filter-out en,$(LANGS)),			\
		$(BUILD_DIR)/info/$(PACKAGE)-$(l).info)			\
		$(BUILD_DIR)/info/$(PACKAGE).info

info: $(INFO_FILES)

# Somehow -M doesn't work well, so use -b
$(BUILD_DIR)/%/info/$(PACKAGE).info: $(SOURCE_FILES)
	$(SPHINX) -b texinfo -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)
	$(MAKE) -C $(@D)

$(BUILD_DIR)/info/$(PACKAGE).info: $(BUILD_DIR)/en/info/$(PACKAGE).info
	mkdir -p $(@D)
	cp $< $@

$(BUILD_DIR)/info/$(PACKAGE)-%.info: $(BUILD_DIR)/%/info/$(PACKAGE).info
	mkdir -p $(@D)
	cp $< $@

# A list of index.html files of multi-file HTML for all $(LANGS)
HTMLI_FILES  := $(foreach l,$(LANGS),					\
		$(BUILD_DIR)/$(l)/html/index.html)
html: $(HTMLI_FILES)

# Somehow -M doesn't work well, so use -b
$(BUILD_DIR)/%/html/index.html: $(SOURCE_FILES)
	$(SPHINX) -b html -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)

# A list of all multi-file HTML files for all $(LANGS)
HTMLM_FILES  := $(foreach l,$(LANGS),					\
		$(foreach f,$(BASE_FILES),				\
		$(BUILD_DIR)/$(l)/html/$(f).html))


# A list of multi-file HTML index.html files for all $(LANGS)
HTMLS_FILES  := $(foreach l,$(LANGS),					\
		$(BUILD_DIR)/$(l)/singlehtml/$(PACKAGE).html)

htmls: $(HTMLS_FILES)

# Single HTML doesn't build its own geninde.html nor search.html
# I keep this code inspired by Debian Policy as a place holder
# This is not used due to bugs apear for Single HTML:
#  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=873456
#  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=876075
#  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=879048
$(BUILD_DIR)/%/singlehtml/$(PACKAGE).html: $(SOURCE_FILES)
	$(SPHINX) -b singlehtml -D language=$* -d $(BUILD_DIR)/$* $(SOURCE_DIR) $(@D)
	mv $(@D)/index.html $(@D)/$(PACKAGE).html
	rm -rf $(@D)/.doctrees
	perl -pli -e 's,href="index\.html#,href="#,g' $@
#	perl -pli -e 's,(genindex|search)\.html,$(PACKAGE).html/$$1.html,' $@

#
# Update all PO files for all languages
#
# In the following, export UTF-8 locale variables to avoid gettext errors.
# Don't simply export at the top of this Makefile to avoid affecting
# the generated localized .js files.
#
# Somehow -M doesn't work well, so use -b
update-po: $(SOURCE_FILES)
	# Let's keep POT files next to PO files
	$(SPHINX) -b gettext -d $(BUILD_DIR)/en $(SOURCE_DIR) $(SOURCE_DIR)/$(LOCALE_DIR)
	@set -ex; for l in $(filter-out en,$(LANGS)); do		\
	    LC_ALL=C.UTF-8 LANG=C.UTF-8 $(SPHINXINTL) update		\
		-d $(SOURCE_DIR)/$(LOCALE_DIR)				\
		-p $(SOURCE_DIR)/$(LOCALE_DIR) -l $$l ;			\
	done
	@echo "Please use 'Update all .po files for changed strings in the English original.' as commit message when committing this to GIT."

# Show translation status
status:
	@set -ex; for l in $(filter-out en,$(LANGS)); do		\
		if [ -d $(SOURCE_DIR)/locales/$$l/LC_MESSAGES ] ; then	\
		    find $(SOURCE_DIR)/locales/$$l/LC_MESSAGES -name 	\
			'*.po' -exec msgfmt -vv {} -o /dev/null \; ;	\
		fi ;							\
	done

# Clean source tree
clean distclean:
	rm -rf $(BUILD_DIR)
	rm -f $(SOURCE_DIR)/$(LOCALE_DIR)/*.pot
	rm -f $(SOURCE_DIR)/$(LOCALE_DIR)/*/LC_MESSAGES/*.mo

install: all
	# Install onto DESTDIR='$(DESTDIR)' for en: text pdf nopdf epub html
	mkdir -p $(DESTDIR)$(datadir)
	[ -e $(BUILD_DIR)/en/text/$(PACKAGE).txt ] &&			\
		cp $(BUILD_DIR)/en/text/$(PACKAGE).txt $(DESTDIR)$(datadir) && \
		gzip -f -9  $(DESTDIR)$(datadir)/$(PACKAGE).txt || true
	[ -e $(BUILD_DIR)/en/pdf/$(PACKAGE).pdf ] &&			\
		cp $(BUILD_DIR)/en/pdf/$(PACKAGE).pdf $(DESTDIR)$(datadir) || true
	[ -e $(BUILD_DIR)/en/nopdf/$(PACKAGE).pdf ] &&			\
		cp $(BUILD_DIR)/en/nopdf/$(PACKAGE).pdf $(DESTDIR)$(datadir) || true
	[ -e $(BUILD_DIR)/en/epub/$(PACKAGE).epub ] &&			\
		cp $(BUILD_DIR)/en/epub/$(PACKAGE).epub $(DESTDIR)$(datadir) || true
	[ -d $(BUILD_DIR)/en/html ] &&					\
		cp -dfR $(BUILD_DIR)/en/html/* $(DESTDIR)$(datadir) || true
	# Install onto DESTDIR='$(DESTDIR)' for non-en: text pdf nopdf epub html
	@set -ex; for l in $(filter-out en,$(LANGS)); do		\
	mkdir -p $(DESTDIR)$(datadir)/$$l ;				\
	[ -e $(BUILD_DIR)/$$l/text/$(PACKAGE).txt ] &&			\
		cp $(BUILD_DIR)/$$l/text/$(PACKAGE).txt $(DESTDIR)$(datadir)/$$l && \
		gzip -f -9 $(DESTDIR)$(datadir)/$$l/$(PACKAGE).txt || true ; \
	[ -e $(BUILD_DIR)/$$l/pdf/$(PACKAGE).pdf ] &&			\
		cp $(BUILD_DIR)/$$l/pdf/$(PACKAGE).pdf $(DESTDIR)$(datadir)/$$l || true ;	\
	[ -e $(BUILD_DIR)/$$l/nopdf/$(PACKAGE).pdf ] &&			\
		cp $(BUILD_DIR)/$$l/nopdf/$(PACKAGE).pdf $(DESTDIR)$(datadir)/$$l || true ;	\
	[ -e $(BUILD_DIR)/$$l/epub/$(PACKAGE).epub ] &&			\
		cp $(BUILD_DIR)/$$l/epub/$(PACKAGE).epub $(DESTDIR)$(datadir)/$$l || true ;	\
	[ -d $(BUILD_DIR)/$$l/html ] &&					\
		cp -dfR $(BUILD_DIR)/$$l/html/* $(DESTDIR)$(datadir)/$$l || true ; \
	done
	# For info
	mkdir -p $(DESTDIR)$(infodir)
	@set -ex; for f in $(INFO_FILES); do				\
	[ -e $$f ] && cp -f $$f $(DESTDIR)$(infodir) || true ;		\
	done
	# singlehtml files
	# don't install Sphinx singlehtml output until various bugs
	# are fixed upstream (e.g. #873456, #876075, #879048)
	#cp $(BUILD_DIR)/en/singlehtml/$(PACKAGE).html $(DESTDIR)$(datadir)
	#@set -ex; for l in $(filter-out en,$(LANGS)); do		\
	#cp $(BUILD_DIR)/$$l/singlehtml/$(PACKAGE).html $(DESTDIR)$(datadir)/$$l ;	\
	#done

.PHONY: all
.PHONY: FORCE_MAKE
.PHONY: text pdf nopdf epub info html htmls
.PHONY: clean distclean update-po
.PHONY: install

#
# GNU make configuration.
#

# If a rule bombs out, delete the target.
#.DELETE_ON_ERROR:

# No default suffixes work here, don't waste time on them.
.SUFFIXES: 
