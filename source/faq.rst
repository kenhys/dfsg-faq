.. _dfsg_and_software_license_faq_draft

Q: What is the purpose of this DFSG FAQ?
----------------------------------------


A: To answer common questions about the Debian Free Software Guidelines and how we judge whether some piece of software is free.


Q: What is debian-legal?
------------------------

A: Debian-legal is a Debian mailing list for the discussion of legal questions related to Debian, including in particular whether some package or prospective package is free software. This usually depends on the license under which it is distributed.

Q: Why does it matter whether something is free software?
---------------------------------------------------------

A: Debian only includes free software. So aside from all its other wonderful properties, if something is not free software (by our standards) we will not include it in Debian.


Q: What makes something free software? Does this depend solely on its license?
------------------------------------------------------------------------------

A: To us, software freedom is a set of rights (free as in speech) rather than a price (free as in beer). There have been a number of attempts to codify these freedoms, which include the freedom to modify and distribute. For our purposes, it must conform with the Debian Free Software Guidelines. The same ideals are expressed in the Open Source Definition and the Free Software Foundation's Four Freedoms.

This almost always depends on the license under which the software is distributed, but there are rare exceptions. When necessary we take other considerations into account. So two packages with the same license could be judged differently based on extra-license comments the copyright holder has made regarding intent or interpretation, or based on how the contents of the package interact with license stipulations.

For a concrete example, the PINE mail client version 3.91 had an MIT-style license, which is generally considered free. The copyright holder told us they wished to interpret the license text in a somewhat counterintuitive fashion: the license allows modification and distribution, but the copyright holder said they interpreted this as allowing modification, and allowing distribution of unmodified copies, but as not allowing distribution of modified copies. We respected their wishes, considered the software non-free, and removed it from Debian.

Q: Which license is best for my new previously-unreleased software? Which should I use? I think it would be fun to make up my own!
----------------------------------------------------------------------------------------------------------------------------------

A: We are computer programmers, so we appreciate the fun to be had by trying to stretch the rules or game the system. And it certainly sounds fun to write your own license! But it is our strong and heartfelt advice that using a tried-and-true license is best for almost all purposes. Even large corporations with dozens of lawyers on staff itching to write their own license have found this out the hard way, as in the Mozilla license saga, or the Djvu license story, or the trouble Trolltech had with the Qt license.

There are many advantages to using standard licenses: they're better understood by the community, were written by actual lawyers and already vetted by the community, people do not have to spend time figuring them out before using the program or helping with development, and they make it much easier to share code between your project and others.

In our opinion (and please do not consider this formal legal advice) ...
    If you want to make sure that everyone who gets a binary, even a modified binary, can get a copy of the corresponding source, then the GNU General Public License, or GNU GPL, is almost certainly your best bet. As a practical matter the GPL has proven amazingly successful, both at encouraging communal development of software and at seeing changes sent back to the originators. (Programs available under the GPL include the Linux kernel, GNU Emacs, GCC, Mozilla, KDE, GNOME, the OpenOffice suite.)
    If you want your code to be reusable in all free software projects, no matter which license they use, and are willing to accept the possibility that somebody may "take it proprietary" (i.e. sell binaries based on modified source without distributing the modified source and without allowing the person who bought a copy of the binary to in turn give a copy to anyone else), you should consider the "new BSD license" (also called the "BSD license without the advertising clause") or the "MIT X license". These BSD-style licenses make it clear that you hold copyright and make no warranty, but that is about it. These licenses are compatible with most licenses, including the GPL. Programs under BSD or BSD-like licenses include the core of the FreeBSD system (both kernel and utilities), X, many networking utilities, and the Apache web server.
    If your code is actually a library or a plugin you might want to consider the GNU LGPL, which allows your code to be linked into proprietary software but keeps your code itself free, and is GPL compatible. (Programs available under the LGPL include BOCHS, many of the GNOME libraries, ADOLC, GLIBC, and libg++.)

Other people who have investigated these issues give similar advice. For instance, see the essay Make Your Open Source Software GPL-Compatible. Or Else. by David A. Wheeler, which includes some compelling statistics.

Q: I've flouted your advice and written a new license. I strongly believe that it conforms to the DFSG and is a free software license. People on debian-legal don't seem to agree though. They give explanations for their decision which I find completely unconvincing. I keep trying to explain the flaws in their reasoning to them, but to no avail. Is there any way for me to compel Debian to accept that my license is free?
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A: No.

Q: I'm writing documentation to accompany a free program. What license should I use for this documentation?
-----------------------------------------------------------------------------------------------------------

A: We strongly suggest you use the same license as used for the program. Then it will be possible to take code and put it into the documentation, and vice versa.

If you would like to grant some extra freedoms for the documentation not granted for the remainder of the software package (eg freedom to distribute as a paper manual without corresponding document source) we recommend you use a dual license: one of which grants these extra freedoms, and the other the same license as the program.

Q: Should I use the GFDL for documentation I write?
---------------------------------------------------

A: The GFDL (version 1.2) seems designed mainly for book-length printed documents rather than digital materials. We would strongly recommend against use of the GFDL v1.2 (GNU Free Documentation License version 1.2), for a number of reasons. A summary of issues with the GFDL was compiled by Manoj Srivastava. If you must use the GFDL for some reason (eg for compatibility), we would very much encourage you to place the material under a dual license, like GFDL/GPL.

It is Debian's hope that a future version of the GNU FDL can be crafted which will address the issues mentioned above, making this question moot.

Q: How can I tell if a license is a free software license, by Debian's standards?
---------------------------------------------------------------------------------


A: The process involves human judgement. The DFSG is an attempt to articulate our criteria. But the DFSG is not a contract. This means that if you think you've found a loophole in the DFSG then you don't quite understand how this works. The DFSG is a potentially imperfect attempt to express what free software means to Debian. It is not something whose letter we argue about. It is not a law. Rather, it is a set of guidelines.

That said, the DFSG is a good start. You might also consider a few thought experiments which we often apply. But do keep in mind that passing some set of tests is not all there is to freeness. These tests are not the final word either: some other tricky bit of nonfreeness might be invented which is not covered by any of our current tests, or something might fail a test as it is currently worded but still be determined to be free software.
    The Desert Island test.

    Imagine a castaway on a desert island with a solar-powered computer. This would make it impossible to fulfill any requirement to make changes publicly available or to send patches to some particular place. This holds even if such requirements are only upon request, as the castaway might be able to receive messages but be unable to send them. To be free, software must be modifiable by this unfortunate castaway, who must also be able to legally share modifications with friends on the island.
    The Dissident test.

    Consider a dissident in a totalitarian state who wishes to share a modified bit of software with fellow dissidents, but does not wish to reveal the identity of the modifier, or directly reveal the modifications themselves, or even possession of the program, to the government. Any requirement for sending source modifications to anyone other than the recipient of the modified binary---in fact any forced distribution at all, beyond giving source to those who receive a copy of the binary---would put the dissident in danger. For Debian to consider software free it must not require any such "excess" distribution.
    The Tentacles of Evil test.

    Imagine that the author is hired by a large evil corporation and, now in their thrall, attempts to do the worst to the users of the program: to make their lives miserable, to make them stop using the program, to expose them to legal liability, to make the program non-free, to discover their secrets, etc. The same can happen to a corporation bought out by a larger corporation bent on destroying free software in order to maintain its monopoly and extend its evil empire. The license cannot allow even the author to take away the required freedoms!

Q: Does the DFSG apply only to computer programs?
-------------------------------------------------

A: No, we apply our standards of freedom to all parts of all software in Debian. This includes computer programs, documentation, images, sounds, etc.

The text of licenses themselves in general need not be free, although legal wording itself is often not subject to copyright and hence effectively in the public domain. Although this is a subject of some controversy within the project, in practice sometimes tiny little snippets of non-free text, generally of historic or humorous or intellectual value, are included (eg /usr/share/emacs/21.3/etc/{JOKES,MOTIVATION}). These should not be integral parts of the package, nor included in a non-removable fashion, nor constitute functional parts of the package such as code or documentation. In general we would suggest avoiding such things, but you do not have to go to enormous trouble to find and root them out. In a similar vein, sometimes relevant scientific papers or technical reports of unclear copyright status are included; although they are not approved, there has, as of yet, been no systematic effort to find and remove such manuscripts.

We do not consider any of this a precedent for the inclusion of non-free code or documentation.

Q: If something is free software according to Debian's standards, do I still face legal risks when I use, modify or distribute it?
----------------------------------------------------------------------------------------------------------------------------------

A: You should take this answer as a total disclaimer of everything.

Even if we were lawyers (which we are not) neither this document nor Debian's acceptance of some license or inclusion of some software should be taken as legal advice. If you need legal advice, you need to hire your own attorney.

We sincerely hope you don't face any serious risks, but our process does not guarantee this. In truth, no process could. As stated above, Debian mainly looks at the license accompanying some software to decide whether it meets the project's standards for free software. This leaves open a number of avenues through which legal problems might conceivably arise, including:
    Some software might have been misappropriated, and a license applied to it without approval of the actual copyright owner. In this case, Debian's evaluation was based on false premises.
    Some software might include code which is copyrighted by third parties and not released under a free software license, or the license terms might be violated (e.g. if they are incompatible with other license requirements). Debian only examines the license and does not formally audit the code, so this cannot be reliably detected, particularly if the problematic code were copied without attribution. (In a few cases such license violations have in fact been found, in particular incompatibilities between requirements for advertising and the GPL.)
    Some software might infringe trademarks. Distributors could potentially be held liable for this in some jurisdictions. Currently, Debian does not check for trademarks and their misuse.
    Some software might infringe patents in jurisdictions in which so-called software patents are allowed. Even though only end users actually run the software, and distributors do not in fact actually engage in the patented process, distributors might be held liable in such a jurisdiction. Debian makes no serious attempt to check for patent violations, and handles this issue in a haphazard and case-by-case fashion. (In fact, checking for this is in practice impossible. If everyone checked for software patents, all software production would grind to a halt.)
    Use or possession of some software might be illegal in some jurisdictions.
    Distribution of some software might be controlled by import or export restrictions in some jurisdictions.
    We might have misread or misinterpreted the license.

Debian's conclusion that a particular computer program is free software, and our choice to distribute it, is an evaluation made for our own purposes. It is not a legal statement on which you can rely, either as a user, software developer, or distributor. We do our best, but we are not lawyers. We are unpaid volunteers. We make no guarantees.

Q: People put the darndest things in their licenses. Could you explain their impact on the freedom of the license?
------------------------------------------------------------------------------------------------------------------

A: Sure, here are some examples:

"Send me a postcard if you like this software
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This makes the license non-free.

Q: But I'd like users of my software to send me a postcard, so I can get a collection of postcards from cool places!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: So-called postcardware, or similarly emailware which requires users to send email to the author, fails the Desert Island test, so it is not free. So no: you can't put this in the license. However we understand your desire to receive postcards from users, and would like suggest another way to achieve this goal. Instead of making this a requirement in the license, make it a personal request. Just add a personal note from the author, which is clearly not part of the license itself, saying "Although it is not required, I'd very much appreciate it if users would send me postcards telling me how they are making use of this program." You should still get postcards, but they will be voluntary. Which is actually nicer, when you think about it.

If you distribute this software, you must pet a cat
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    This makes the license non-free, and is also cruel to people who are allergic to cats.

You may modify this software, but all bug fixes must be sent to the author
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    This makes the license non-free. (But a request rather than a demand is fine.)

Q: Are clickwrap licenses okay? (Meaning licenses require anyone receiving the software to click on an "I AGREE" button indicating ascent to the terms.)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: No, not unless the clickwrap stuff can be removed. Even aside from freeness, as a practical matter such a clickwrap requirement would be an unreasonable burden upon our users.

    To be technical, in principle one could put the GPL in a clickwrap and the license would be perfectly fine. But once you add a requirement that the software must be distributed via the clickwrap, or that clickwrap code cannot be removed from the software, your license becomes non-free. Since clickwraps without such a requirement are a bit pointless, clickwrap licenses are almost always non-free.

Q: I've just made up a new license which requires people using the software to agree to a contract which forbids them from doing some bad stuff (like finding security flaws and not reporting them) that copyright law would otherwise allow as fair use. We can force everyone to cooperate even more! Isn't that a great idea?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: We do not think so. Such a license does not meet our standards of freedom. Freedom means not only the freedom to modify, and to help others; it also means the freedom to enjoy one's own privacy.

Q: I'm a working scientist, and would like to release code implementing my work. However I want to make sure that people using the software mention its use, and cite my papers, in papers they write. Should I include this in the license?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: You have a valid concern. Computer scientists often receive inadequate credit for their scientific contributions. But putting such a clause in the license would render your software non-free. Instead we suggest a note, not part of the license itself, reminding users of the rules of scientific propriety. Eg:

    SCIENTISTS: please be aware that the fact that this program is released as Free Software does not excuse you from scientific propriety, which obligates you to give appropriate credit! If you write a scientific paper describing research that made substantive use of this program, it is your obligation as a scientist to (a) mention the fashion in which this software was used, including the version number, with a citation to the literature, in the Methods section, to allow replication; (b) mention this software in the Acknowledgements section. The appropriate citation is: Robert B. Laub (2003) "BLOBBER: A program that blobs", Blobbing Bulletins 12(34):567-89. Moreover, as a personal note, I would appreciate it if you would email bobblaub@ubl.edu with citations of papers referencing this work so I can mention them to my funding agent and tenure committee.

Q: Can I say "Users of this software must ..."?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: Stop right there. Free software can't have any restrictions on use. (Under US copyright law it's not clear that it is even possible to restrict use, once someone has a legal copy.) We can only consider restrictions on distribution. So your license can't say "users must bathe regularly" or "users must tell me about particularly noteworthy uses" or "users must smile at someone who looks sad" or impose any usage condition whatsoever.

Q: Can I say "You must not charge [much] money for distributing the program"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free. We want Debian to be distributed by for-profit CD vendors and improved by corporate resellers. We can only include programs whose authors allow this.

    For many users buying Debian on disks is more convenient (and cheaper) than downloading. For-profit distribution is the most reliable and convenient way to ensure that Debian disks are easily available everywhere where there is a demand. Because everyone can download the CD images and start producing their own disks, competition ensures that nobody will make an undeserved fortune distributing Debian.

Q: Can I say "You must not use the program for commercial purposes"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    A: This is non-free. We want businesses to be able to use Debian for their computing needs. A business should be able to use any program in Debian without checking its license.

Q: Can I say "You must not change the program such that it does not implement what I say is the correct interface"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because it denies the user the freedom to adapt the software to a different problem which requires different interfaces.

    We consider a very important facet of software freedom to be the freedom to adapt old tools to new problems. If I find a program in Debian which I think can be adapted to solve a problem I have (and which nobody ever thought about before), I expect to be allowed to make that adaptation---even if that means I need to change some of the external interfaces of the program. And I should not need to worry about the fact that my adapted tool does not solve the original problem anymore if the original problem is not what I need to solve.

    Additionally: This kind of clause gives the author (authors are free to ignore their own rules) a de facto monopoly on experiments with alternative ways of doing things. It therefore fails the Tentacles of Evil test. One of the points of free software is that everyone should be free to try out new ways of doing things. This license clause denies the users the freedom to try out and exchange new ideas. This freedom is one of the most important driving factors for progress in computing---and we like progress.

Q: Can I say "You must only distribute the program to people who have agreed to this license"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because it makes it hard to mass distribute the program together with other programs, for example on an FTP site or CDs. Such clauses effectively forbid FTP distribution, and CD distribution would be prohibitively expensive and inconvenient if the CD manufacturer was required to make every customer sign a zillion different licenses. (See clickwrap above.)

Q: Can I say "BY DOWNLOADING THIS SOFTWARE YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS LICENSE"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: There are certain rights granted to anyone who is in lawful possession of a copy of a work, even when that work is under copyright. Free software gives you those rights (at least as they hold in the United States, where this is called fair use) plus a whole bunch more. Anything that tries to get the user to agree to "be bound by" something is almost certainly doing so in order to get them to agree to give up some of the rights they would otherwise automatically enjoy. After all, one does not need the user's agreement in order to give them extra rights. So boilerplate like the above is generally a promise that the license will, upon close examination, be found to contain something non-free. In other words, free software licenses do not need such an agreement.

Q: Can I say "If you modify the program [and distribute your modifications] you must send your patch to me"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because it fails the Desert Island test and also the Dissident test.

Q: Can I say "I reserve my right to withdraw your license if anyone claims they have copyright [or patent rights] to the program"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because (among other things) it fails the Tentacles of Evil test. However it would be okay to say "use of this software is at your own risk; this includes the risk of violating the patents or copyrights of third parties, which is the sole responsibility of the user and for which the software's authors are not responsible."

Q: Can I say "You must obey U.S. export laws"?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because it imposes restrictions on people outside the US which they might otherwise not be subject to. To protect yourself while keeping the license free you can rephrase this as a warning instead of a condition: "Please be aware that this license does not release you from your obligation to follow the law. We note in particular the US Export laws which may impact what you are legally allowed to do with this software, especially with regard to redistribution."

A stronger way to phrase this is: It is not the job of a copyright license to reiterate what is or is not legal in a particular jurisdiction. The job of a copyright license is to grant permissions to do things that would otherwise be forbidden under copyright law.

Q: Can I require users or distributors to check for updates using something like "You must monitor my website"? It is for their own protection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: This is non-free because it fails the Desert Island test. It is also an unreasonable burden---imagine if you had to constantly monitor 400 web sites in order to legally use the software on your computer.

Q: I understand your logic, but my software is special and I'm really a very nice corporation and I have very innocuous and socially beneficial reasons for wanting to include a very small extra technically non-free clause that is in fact not so inconvenient as you seem to think. Plus this software was very expensive to develop, and is truly wonderful, and I'm trying very hard to contribute to the free software community in a way that is acceptable to my lawyers. Could you please make an exception just this once? You should at least compromise a little bit, because I have been very flexible on many other points.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A: No.

There are over 10,000 packages in Debian. If we made exceptions for just 1%, users would have to carefully evaluate how much of a burden the non-free parts of 100 licenses might be. It is simply not feasible, and we think we've drawn the line at the right place: where it is best for our users and for the free software community. (In any case, these matters are not subject to compromise. Think satisfying the fire code rather than negotiating a deal.)

    As a bit of practical advice, you would really be better off using a standard license. It might be less fun for your lawyers, but your software will be more readily accepted, and more people will contribute to it. Isn't that what you want?

Q: What does the DFSG mean by no discrimination? Doesn't the GPL discriminate against companies making proprietary software
---------------------------------------------------------------------------------------------------------------------------

A: The intent is to prevent prohibitions against use by people fighting their own government, or building weapons of mass destruction, or Jews, or the French Postal Service. The DFSG contains a few more examples. The GPL does not discriminate against companies that want to make proprietary software based on GPLed code because they are given the same rights to GPLed software that anyone else has. They happen to also want the right to sell non-free derivative works, but no one is given that right so this does not constitute discrimination.

Q: What about licenses that grant different rights to different groups? Isn't that discrimination, banned by DFSG#5/6?
----------------------------------------------------------------------------------------------------------------------

A: For Debian's purposes, if all the different groups can exercise their DFSG rights, it is OK if there are other people who can do more. For example, if a work were distributed to everyone under the GPL, but elementary school teachers were given the extra right to distribute binaries without distributing the corresponding source code, it would still be DFSG-Free.

Q: Since software "placed in the public domain" has no license isn't it not under a free software license, and therefore not acceptable as free software according to the DFSG?
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A: Software placed in the public domain has all the freedoms required by the DFSG, and is free software.

Q: The program FOO is free according to the DFSG and its license; can I now demand that Debian package FOO and include it in Debian GNU/Linux?
----------------------------------------------------------------------------------------------------------------------------------------------

A: Although Debian includes only free software, we do not include all the free software in the whole world. (Although we do include so much that one can understand how people might think we include it all.) What software we choose to distribute is Debian's own decision, and no one else's. In particular, we are not obligated to distribute FOO.

Here is what must occur for FOO to get into Debian GNU/Linux. First, it must be free, by our standards. Then it must be properly packaged, either by a Debian developer or by someone else. Then it must be uploaded to the Debian servers by a Debian developer. (This is called sponsoring if someone else actually did the packaging.) Then the Debian ftp masters must allow it in; they are a final screen against license issues or software integration problems. At this point the package is being distributed by Debian, but is not part of the official release. For that to occur it must be of sufficiently high quality to make it through a semi-automatic QA (Quality Assurance) process involving the Debian BTS, and the release manager must allow it to be included in the next major release.

To initiate this process you can file an RFP. See Work-Needing and Prospective Packages for details.

Q: What are compatible licenses, and what does GPL compatible mean?
-------------------------------------------------------------------

A: In order for two licenses to be compatible it must be possible to mingle code under both licenses in a new work. When this is done the result is that the terms of both licenses must be met for the work as a whole. The GPL makes this feature of copyright law explicit by stating that if you cannot for any reason (e.g., because of another license) meet the terms of the GPL, the GPL grants no rights at all. In order for a license to be GPL compatible, then, you must be able to meet both the terms of the GPL and the other license simultaneously.

Q: What is a dual license?
--------------------------

A: When a work is released under a dual license the recipient is explicitly given the option to choose which license to apply to a derivative work. Someone making modifications may include new code under either license, or (as is most common) under the same dual license.

Q: Why in most dual licensed software is the GPL one of the licenses?
---------------------------------------------------------------------


A: The GPL is particularly common in dual licenses because it allows the code to link with the large body of GPLed code available including many important libraries, and to be incorporated into other GPLed works. Many common works are under dual GPL/xxx licenses: perl is under GPL/Artistic, Qt is under GPL/QPL, Mozilla is under GPL/MPL. This is almost always due to a project starting with a home-brewed GPL-incompatible license, then realizing they'd made a mistake and finding relicensing in this fashion to be the most convenient resolution.

Q: Some programs distributed under the GPL read "... under the ... GPL ... either version 2, or (at your option) any later version." Others leave off the last clause, ending "GPL version 2." Why is this? Is code distributed "GPLv2 or later" compatible with code distributed under simply "GPLv2"?
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


A: This is an example of a dual license. Thus code distributed under "GPLv2 or later" can, at the redistributor's option, be redistributed instead under simply "GPLv2". A combined work containing both "GPLv2 or later" code and plain "GPLv2" code can only be redistributed under a plain "GPLv2".

It is considered polite to retain the "or later" clause when a program using it is modified, if at all possible.

The intent of the "or later" clause is to make a "license upgrade" easier, i.e. to allow the Free Software Foundation to fix any bugs or loopholes that might in the future be found in the GPLv2. Without this clause, moving a GPLv2 program to some future GPLv3 would require the permission of the copyright holders of all contributed code. This typically would include not just individuals but also their employers and former employers, some of whom may have gone through bankruptcy or mergers and had their assets, including copyrights, acquired by enormous multinationals or companies that specialize in liquidation. It can be very difficult to even find all the actual copyright holders and appropriate contact points, leave alone to explain the situation and convince them all to agree to a license change.

Q: What is an almost-free license?
----------------------------------

A: This is a license which seems written with the intent of making the software free, but with some problem that stymies this goal. Clauses like "only a reasonable fee may be charged for distribution" are typical; see above.

Q: Do people really release programs under almost-free licenses?
----------------------------------------------------------------

A: Unfortunately yes. For instance iozone3, povray, or rar. Programs under almost-free licenses are typically, with time, either re-released under a free license (e.g., the Squeak Smalltalk system, or ckermit) or are replaced by superior free alternatives (e.g., xlock, superseded by the free xscreensaver.)

Q: What license does the Free Software Foundation (which sponsors the GNU project) advise?
------------------------------------------------------------------------------------------

A: The GPL. The FSF has declared the LGPL obsolete, and urges libraries to be released under the GPL unless there is a compelling reason to use the LGPL.

Q: The FSF project asks for copyright assignments of all modifications submitted for inclusion in their projects. Does this put software at risk if the FSF should be acquired or infiltrated by a corporate enemy of free software, or lose a court battle and have its assets confiscated?
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


A: No. The FSF has clever lawyers who wrote the copyright assignment carefully to preclude any such danger.

Q: What does GNU LGPL stand for?
--------------------------------

A: The LGPL stands for Lesser General Public License. (It used to stand for Library General Public License, but no longer.)

Q: Doesn't even Debian include some non-free software?
------------------------------------------------------
A: No.

We do allow some non-free packages to use our distribution infrastructure, but they are not actually part of Debian proper. (There are some requirements on licenses for such non-free programs, so debian-legal does sometimes examine the licenses of non-free programs for distributability; it is not a priority though.) The existence of this non-free area can lead to confusion, so some members of Debian would like to remove non-free from our servers. On the other hand, many packages in non-free have migrated into Debian proper when their authors changed their almost-free licenses to make them actually free, and some have had their functionality painlessly migrated to newly available free alternatives. For this reason, other Debian developers want to retain the status quo.

Q: What is the story with KDE and Debian and some license problem? I heard that Debian hates KDE.
-------------------------------------------------------------------------------------------------

A: KDE is currently in Debian. There is no license problem. We love KDE and always have, and included it in Debian the moment we were able to.

Some time ago there was a license problem, but it has been resolved. The problem was that KDE was under the GPL while the Qt library, which it relied on, was under a rather odd license called the QPL, which although (debatably) free was not GPL compatible. (And before that, Qt was under a license that didn't allow modifications at all.) This meant that Debian did not have permission to distribute KDE, at least as pre-compiled binaries, without a "Qt waiver" on all the KDE code, which we did not have. Debian was thus (very reluctantly) unable to distribute KDE. This license issue caused a fuss, and the result was (a) GNOME, and (b) TrollTech re-released the Qt library under a QPL/GPL dual license, which resolved the issue.

Q: What is a waiver on a GPL-licensed program?
----------------------------------------------

A: It is generally permission to create and distribute derived works that are linked to some particular library which is under a non-GPL-compatible license. For example, libssl is such a library and some GPLed programs use libssl. In order for such a program to be included in Debian, a note accompanying the license giving some extra permission must be present. Here is one such note (taken from /usr/share/doc/wget/copyright) which allows binaries of the GPLed program wget which are linked to the non-GPL-compatible OpenSSL library to be distributed:

    In addition, as a special exception, the Free Software Foundation gives permission to link the code of its release of Wget with the OpenSSL project's "OpenSSL" library (or with modified versions of it that use the same license as the "OpenSSL" library), and distribute the linked executables. You must obey the GNU General Public License in all respects for all of the code used other than "OpenSSL". If you modify this file, you may extend this exception to your version of the file, but you are not obligated to do so. If you do not wish to do so, delete this exception statement from your version.

Q: If I release software under a free software license that does not allow others to make proprietary derived works, does this preclude me from making proprietary derived works myself?
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A: No.

Licenses gives others permissions that you already have. It is your code. You don't need your own permission to make a proprietary version, or to release a version under a different license, or to sell someone the right to make a proprietary version, or to sell someone the right to incorporate parts of your code in a proprietary program. (One caveat: if you incorporate non-trivial changes other people have made into your code base you are no longer the sole author. You would then need their permission to make a proprietary version, just as they would need yours.)

Q: I want to release my code as free software, but am willing to allow people to pay for the right to include it in their proprietary programs. Should I say so in the license?
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

We believe it is better (ie simpler and less confusing for everyone) to mention that in a separate note, rather than in the license. This allows you to use a standard free software license. It also tends to make the offer easier to see, since it would be buried if placed in a long document full of legalese.

Q: What is the difference between free software and open source software?
-------------------------------------------------------------------------

A: There is no difference, or at least there isn't meant to be.

"Open source software" was coined as a new name for "free software" which was intended to avoid the confusion arising from the fact that you're allowed to charge money for free software which can seem counterintuitive. It was also meant to give the community some branding power, because it was hoped that "open source software" could be controlled and legally permitted for use only on actual free software, whereas any company can distribute some proprietary software at no charge (eg Internet Explorer) and, since they are not charging any money, call it "free software". (What we mean by the "free" in "free software" is of course a set of freedoms, not a price; "free as in free speech" rather than "free as in free beer".) OSS was also meant to sound more professional and hence more attractive to businesses. In practice there are slight differences in emphasis between the people who use the two terms, and having two terms has caused confusion and a surprising amount of friction. Some people have begun to use terms like FLOSS (Free/Libre Open Source Software) to avoid both possible confusion and taking a side in the terminological debate.

Some of the people who helped coin and popularize the term "open source software" have had second thoughts about it, and some of the legal measures originally planned (formal registration of the term "open source" as applied to software) did not actually come to fruition. The term "open source" is itself ambiguous, in that some companies "open" their source code for examination without granting the right to make changes or to pass on copies. The organization controlling the "open source software" certification mark made some borderline (ie controversial) determinations about some licenses, which served to dilute the standing of the mark itself.

A number of people who made enormous contributions to Debian (eg Bruce Perens, former Debian project leader) and to free software in general have or held prominent positions in the Open Source Initiative. Nonetheless Debian in general, and this document in particular, uses the term "free software". This is in part out of respect for the Free Software Foundation and the GNU project, in part to emphasize the large body of GNU code in Debian GNU/Linux, in part because the term "free software" seems more appropriate in technical forums because it sounds less corporate-speak, and in part due to the issues discussed above.

Q: What is the difference between commercial and proprietary software?
----------------------------------------------------------------------

A: To quote the 1913 Webster's,

    {Proprietary articles}, manufactured articles which some person or persons have exclusive right to make and sell.
    --U. S. Statutes.

    {Commercial}. Of or pertaining to commerce; carrying on or occupied with commerce or trade; mercantile; as, commercial advantages; commercial relations. "Princely commercial houses."
    --Macaulay.

Proprietary software is software that can be legally modified or distributed only by some authorized set of persons. So by definition proprietary software cannot be free software.

Software is commercial if some corporation is distributing it and trying to make money from it, either via direct sales or via maintenance and support. Many corporations have made money by distributing, supporting, and maintaining free software, so it is possible for a piece of software to be both free and commercial. Anyone can sell a support contract for free software, and access to the source code allows such support to include not just hand-holding but also bug fixes and new features. This is a major advantage of free software as it makes for competition in such services, which is greatly to the benefit of users of free software. Corporations offering support contracts for free software include IBM, Redhat, LinuxCare, SAP, Trolltech, Ximbiot, and many others. Debian also encourages corporations to create products based on Debian GNU/Linux, such as Ubuntu. Many corporations pay employees to write and maintain free software from which they fully intend to make money, some of whom directly contribute their work to Debian as part of their jobs. Other employees of corporations are paid to maintain free software which is in internal use, and to contribute their modifications thus ensuring that their changes will not have to be re-applied to each new version.

Q: If all software were free how could programmers make a living?
-----------------------------------------------------------------

A: This question is extremely misleading. There is no reason to think that free software puts programmers in the poorhouse. In fact, the economics of the situation argue for quite the reverse! The vast majority of working computer programmers (over 95%) work at businesses that do not sell software: they write software for in-house use, for embedded devices, for driving new hardware, etc. Free software makes programmers more productive, which should have the effect of raising salaries. It also makes programming more fun, since there is less need to re-invent the wheel. Moreover, even if free software reduced society's demand for programmers well below the current supply, so what? Automation and increased efficiency often reduce the number of jobs in some category and this is something we accept---buggy whip manufacturers come to mind---and Debian is not a trade union.

Debian itself takes no stand on the "morality" of proprietary software, or whether aspects of the current legal system which encourage proprietary software should be modified. Many people who write free software, including some Debian developers, also write proprietary software.

Q: What are the FSF's four freedoms?
------------------------------------

A: These four freedoms:
    The freedom to run the program, for any purpose (freedom 0).
    The freedom to study how the program works, and adapt it to your needs (freedom 1).
    The freedom to redistribute copies so you can help your neighbor (freedom 2).
    The freedom to improve the program, and release your improvements to the public, so that the whole community benefits (freedom 3).

are the Free Software Foundation's articulation of what it believes all software users deserve. (Note that full exercise of Freedoms 1 and 3 requires access to the source code.) They are elegantly phrased, and arguably an improvement in some ways on the earlier DFSG. However they refer to exactly the same set of freedoms as the DFSG. If a license is inconsistent with the FSF's four freedoms, you can be sure that Debian will also consider it non-free.

The term "four freedoms" is a play on words over the influential "four freedoms" speech of US President Franklin Delano Roosevelt in which he outlined the following four freedoms: Freedom of Speech, Freedom of Religion, Freedom from Want, and Freedom from Fear.

Q: Why does Debian apply the DFSG to documentation?
---------------------------------------------------

A: Debian applies the same standards of freedom to all works it distributes; some of these standards are written down in the DFSG. No widely-accepted reasons have been provided to use different standards for documentation than for code.

Even if we were to treat code and documentation differently, first we would need to have a clear way to tell documentation and code apart. Many works, like source code annotated with Javadoc comments or Postscript files, are programs and documentation at the same time, so it is very hard to find such a clear division.

Q: Shouldn't Debian allow documents which describe standards to be non-modifiable? Why should we need the same freedoms as for code?
------------------------------------------------------------------------------------------------------------------------------------

A: We have three reasons: such a restriction is unnecessary; it is useless; and it is not true that it would be less appropriate for code than for documentation.

First, misrepresentation can be prevented without forbidding anyone to modify the work, by requiring all modified works to not claim that they are the original work or that they were written by the original authors; so, the restriction is unnecessary.

Furthermore, a clause in a copyright license would not prevent someone misrepresenting the work or its authors. For example, I might create a new, original document titled "RFC 2821, Simple Mail Transfer Protocol" with a distorted description of SMTP, and with this action I would not be contravening the license of the IETF's RFC 2821. The proper defense against this are the various laws dealing with libel, fraud and impersonation. So, such a restriction would be useless.

Finally, if there were any reasons to allow such a restriction in documentation of standards, these reasons would allow it in programs too. For example, a standards document might be accompanied by a demonstration program. One could say that the reputations of the authors of the document and the program may suffer if someone breaks either one of them. If Debian allowed any restriction on modification of the document, Debian should also allow the same restriction on modification on the program, so this kind of restrictions would not be more appropriate for documentation than for programs.

Q: Why isn't there a DFDG, "Debian Free Documentation Guidelines", to complement the DFSG?
------------------------------------------------------------------------------------------

A: A number of people have proposed this idea, but have not met with any success to date. Serious consideration of adopting a DFDG entails someone actually writing one; this hurdle is surprisingly difficult. At that point, the obvious question that will be asked is why the proposed DFDG differs from the DFSG. In order to make a reasonable case, it seems necessary to justify each license restriction permitted by the proposed DFDG but not by the DFSG. Furthermore, it would be sensible to show how to distinguish which packages should be covered by the DFDG rather than the DFSG; why each particular restriction relaxed by the DFDG should be relaxed; and why they should be relaxed only for documentation but not for other software components, like code.

Q: If the DFSG is to be applied to documentation as well as to programs, why is the text of the GPL included in Debian, if it says that it cannot be modified at all?
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

A: Because the verbatim text of the license must be distributed with any work licensed under its terms. This is not specific to the GPL; almost all free licenses require that their text be included verbatim with the work. As a compromise, Debian distributes copies of the GPL and other licenses under which the components of Debian are covered. This compromise will not be extended to other types of works.

(Note that according to the FSF, which is the author of the GPL, you're actually allowed to modify the text of the GPL and create a derived license if you remove the preamble and you do not call the results "General Public License." See the GNU GPL FAQ for more information.)

Q: What FAQs remain to be added to this document?
-------------------------------------------------

A: What fraction of Debian packages/lines-of-code are under what licenses? What is an almost-free license (eg xlock's) and why should I avoid them? Some historical material, both summaries and pointers to longer treatments, would be nice. Some references. More links, both cross-references and references to other documents, would be useful.

Q: Who wrote this document?
---------------------------

A: Barak A. Pearlmutter (bap@debian.org) with help from Joe Moore (joemoore@iegrec.org), Mark Rafn (dagon@dagon.net), Thomas Bushnell, BSG (tb@becket.net), Richard Braakman (dark@xs4all.nl), Henning Makholm (henning@makholm.net), Anthony Towns (aj@humbug.org.au), Jeremy Hankins (nowan@nowan.org), Florian Weimer (fw@deneb.enyo.de), Thomas Hood (jdthood@yahoo.co.uk), James Devenish (j-devenish@users.sourceforge.net), Glenn Maynard (glenn@zewt.org), Jacobo Tarrío (jtarrio@trasno.net), Andrew Suffield, Doug Jensen, Francesco Poli, Anthony DeRobertis, Raul Miller, Evan Prodromou, Ben Finney, Humberto Massa, MJ Ray.

