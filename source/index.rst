.. DFSG FAQ documentation master file, created by
   sphinx-quickstart on Wed Sep  2 20:43:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DFSG and Software License FAQ (Draft)
=====================================

Licensed by CC BY-SA 4.0

Original version is here: https://people.debian.org/%7Ebap/dfsg-faq.html

This FAQ is composed by Barak A. Pearlmutter (bap@debian.org) with help from many contributors.

Contributor
***********

* Joe Moore (joemoore@iegrec.org)
* Mark Rafn (dagon@dagon.net)
* Thomas Bushnell
* BSG (tb@becket.net)
* Richard Braakman (dark@xs4all.nl)
* Henning Makholm (henning@makholm.net)
* Anthony Towns (aj@humbug.org.au)
* Jeremy Hankins (nowan@nowan.org)
* Florian Weimer (fw@deneb.enyo.de)
* Thomas Hood (jdthood@yahoo.co.uk)
* James Devenish (j-devenish@users.sourceforge.net)
* Glenn Maynard (glenn@zewt.org)
* Jacobo Tarrío (jtarrio@trasno.net)
* Andrew Suffield,
* Doug Jensen
* Francesco Poli
* Anthony DeRobertis
* Raul Miller
* Evan Prodromou
* Ben Finney
* Humberto Massa
* MJ Ray

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   faq

